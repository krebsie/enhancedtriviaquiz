package beans;

/*
 * Adjust the package name to match your top-level package.
 */
//package src.java.com.corejsf;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import sql.SQL;

/**
 * The database bean provides access to the database.
 * The current implementation is just a stand-in for a future database.
 * The future database bean will have the same methods plus a few more when
 * the apllication is enhanced to keep track of scores for different users.
 * 
 * Beans that invoke database bean methods should declare a database variable as
 * follows:
 * 
 * @Inject private Database database;
 * 
 */
@Named(value = "database")
@ApplicationScoped


public class Database {
    
    @Resource(name = "jdbc/triviaQuiz9")
    private DataSource triviaQuizSource;
    
    /**
     * users is a Map of userNames and passwords.
     */

  private Map<String, List<Problem>> problems;
  private Map<String, Map<String, Integer>> scoresMaster;
  private Map<String, Integer> scoresByCategory;
  private Map<String, String> users;
  private List<Integer> currentScores;
  private Integer score;
  

  
    private List<String> buildList(ResultSet resultSet, String columnName)
      throws SQLException {
    List<String> list = new ArrayList<>();
    while (resultSet.next()) {
      list.add(resultSet.getString(columnName));
    }
    return list;
  }
    
      private User buildUser(ResultSet resultSet)
      throws SQLException {
    resultSet.next();
    String username = resultSet.getString("username");
    String password = resultSet.getString("password");

    return new User(username, password);
  }
  
  public void addUser(User userBean) throws SQLException {
      if (users == null) {
          users = new HashMap<String, String>();
      }
      //if (users.containsKey(userBean.getUsername())) {
          users.put(userBean.getUsername(), userBean.getPassword());
      //}
      Connection triviaQuizConnection = null;
      Statement statement = null;
      try {
       String sql = "INSERT INTO users " + "VALUES (" + userBean.getUsername() + ", " + userBean.getPassword() + ")";
       triviaQuizConnection = triviaQuizSource.getConnection();
       statement = triviaQuizConnection.createStatement();
       statement.executeUpdate(sql);
        } catch (SQLException se) {
            // Handle errors for JBDC
            se.printStackTrace();
        }
  }
  
  public User getUser(String username) throws SQLException, IOException {
      String query = SQL.getSQL("users-query");
      try (Connection triviaQuizConnection = triviaQuizSource.getConnection()) {
          PreparedStatement statement = triviaQuizConnection.prepareStatement(query);
          statement.setString(1, username);
          ResultSet resultSet = statement.executeQuery();
          return buildUser(resultSet);
      }
  }

 
    public void setCurrentScores(List<Integer> curScores) {
//      FacesContext facesContext = FacesContext.getCurrentInstance();
//      User userBean = (User) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, "userBean");
//      currentScores = Arrays.asList(scoresMaster.get(userBean.getUsername()).values().toArray(new Integer[0]));
        this.currentScores = curScores;
  }

 
  public List<Integer> getCurrentScores() {
//      FacesContext facesContext = FacesContext.getCurrentInstance();
//      User userBean = (User) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, "userBean");
//      return Arrays.asList(scoresMaster.get(userBean.getUsername()).values().toArray(new Integer[0]));
        return currentScores;
  }
  
  public String getPassword(User userBean) {
      return users.get(userBean.getUsername());
  }
  /**
   * addProblem(problem) adds problem to the list of problems for the category
   * named problem.categoryName.
   * @param problem the problem to be added
   */
  private void addProblem(Problem problem) {
    if (problems == null) {
      problems = new HashMap<String, List<Problem>>();
    }
    String categoryName = problem.getCategoryName();
    List<Problem> category = problems.get(categoryName);
    if (category == null) {
      category = new ArrayList<Problem>();
      problems.put(categoryName, category);
    }
    category.add(problem);
  }
  
  
  /**
   * addScore(String username, String categoryName, int score) add scores to the 
   * Database. The outer Map is keyed by user name, the inner Map is keyed by 
   * categoryName. Map<String, Map<String, Integer>>
   * @param userBean the current user, used to access the username
   * @param quizBean the current problem bean used to access the quiz score and categoryBane
   */
    public void updateScore(User userBean, QuizBean quizBean) throws SQLException, IOException {
        if (scoresMaster == null) {
            scoresMaster = new HashMap<>();
        }
        String userName = userBean.getUsername();
        if (!scoresMaster.containsKey(userName)) {
            scoresByCategory = new HashMap<String, Integer>();
            for (String category : getCategoryNames()) {
                scoresByCategory.put(category, 0);
            }
            //scores.put(userName, )
        }
        //String categoryName = quizBean.getCurrentCategory();
        //Integer score = quizBean.getScore();
        scoresByCategory.put(quizBean.getCurrentCategory(), quizBean.getScore());
        scoresMaster.put(userName, scoresByCategory);
     String query = SQL.getSQL("update-score-query");
        try (Connection triviaQuizConnection = triviaQuizSource.getConnection()) {
            Statement statement = triviaQuizConnection.createStatement();
            String sql = "UPDATE score " + "SET correctAnswerCount = " + quizBean.getScore() + " WHERE users in (users) AND categoryNAME in (category)";
            statement.executeUpdate(sql);
              // Now you can extract all the records
      // to see the updated records
      sql = "SELECT users, category, correctAnswerCount, FROM score";
      ResultSet rs = statement.executeQuery(sql);

      while(rs.next()){
         //Retrieve by column name
         String username  = rs.getString("users");
         String category = rs.getString("category");
         int correctAnswerCt = rs.getInt("correctAnswerCount");
         String first = rs.getString("first");
         String last = rs.getString("last");

         //Display values
         System.out.print("username: " + username);
         System.out.print(", correctAnswerCount: " + correctAnswerCt);
          System.out.println(", category: " + category);
         System.out.print(", First: " + first);
         System.out.println(", Last: " + last);
      }
      rs.close();
        } 
    }
    
    public void setScoresMaster(Map<String, Map<String, Integer>> scores) {
        this.scoresMaster = scores;
    }
    
    public Map<String, Map<String, Integer>> getScoresMaster() {
        return scoresMaster;
    }
    
      public List<String> getScores(String username) throws SQLException, IOException {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        User userBean = (User) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, "userBean");
//    
//      return Arrays.asList(scoresMaster.get(userBean.getUsername()).values().toArray(new Integer[0]));
        String query = SQL.getSQL("score-query");
        try (Connection triviaQuizConnection = triviaQuizSource.getConnection()) {
            PreparedStatement statement = triviaQuizConnection.prepareStatement(query);
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            return buildList(resultSet, "score");
        }
      }
     
  

  /**
   * Creates the database bean.
   */
  public Database() {
    // Add your own trivia quiz problems here by invoking addProblem().
    // The argument for invokation should be an invokation of a Problem
    // constructor like the following:

    addProblem(new Problem("Computer Science", "If you need to sort a"
              + " very large list of integers (billions), what efficient sorting "
              + "algorithm is your best bet?", new String[]{"Quicksort", "Mergesort", "Radix Sort", "Bubblesort"}, "Quicksort"));
      addProblem(new Problem("Mathematics", "What does pi equal?",  new String[]{"3.15490", "3.14679", "3.14159", "3.14169"}, "3.14159"));
      addProblem(new Problem("Physics", "What is equal to the product of mass and acceleration?", new String[]{"Force", "Momentum", "Density", "Volume"}, "Force"));
      addProblem(new Problem("Astronomy", "What is the name of the most prominent crater on the Moon?", new String[]{"Tycho", "Copernicus", "Kepler", "Galileo"}, "Tycho"));
      addProblem(new Problem("Computer Science", "Which of the following is a means of compressing images by blurring the boundaries between different colors while maintaining all brightness information?", new String[]{"JPEG", "LZW", "MIDI", "GIF"}, "JPEG"));
      addProblem(new Problem("Computer Science", "Which of the following is the binary representation of 4 5/8?", new String[]{"110.101", "100.101", "10.011", "100.11"}, "100.101"));
      addProblem(new Problem("Computer Science", "Which of the following is a major standardization organization within the U.S.?", new String[]{"LZW", "ISO", "ANSI", "ASCII"}, "ANSI"));
      addProblem(new Problem("Computer Science", "When users are involved in complex tasks, the demand on __________ can be significant.", new String[]{"Short-term memory", "Shortcuts", "Long-term memory", "Their ability to focus"}, "Short-term Memory"));
      addProblem(new Problem("Astronomy", "When there are two full moons in the same calendar month, the second is called a \"Blue Moon\". How often does this happen on average? Every ________________ years.", new String[]{"1 1/2 to 2", "2 1/2 to 3", "3 1/2 to 5", "5 1/2 to 10"}, "2 1/2 to 3"));
      addProblem(new Problem("Astronomy", "What color is on the outside of the arc of a rainbow?", new String[]{"White", "Violet", "Green", "Red"}, "Red"));
      
      users = new HashMap<String, String>();
      scoresMaster = new HashMap<>(); 
      
      
  }

    public Integer getScore(QuizBean qb, String categoryName) throws SQLException, IOException {
        List<String> scores = getScores(qb.getCurrentUser().getUsername());
        
      return scoresMaster.get(qb.getCurrentUser().getUsername()).get(categoryName);
          
    }
  
  /**
   * database.getProblems(categoryName) returns the list of problems in the
   * database whose category name is categoryName.
   * @param categoryName
   * @return a list of problems
   */
  public List<Problem> getProblems(String categoryName) {
    return problems.get(categoryName);
  }

  /**
   * database.getCategoryNames() returns the list of category names in the
   * database.
   * @return the list of categoryNames
   */
  public List<String> getCategoryNames() throws SQLException, IOException {
    //return Arrays.asList(problems.keySet().toArray(new String[0]));
    String query = SQL.getSQL("category-query");
    try (Connection triviaQuizConnection = triviaQuizSource.getConnection()) {
        PreparedStatement statement = triviaQuizConnection.prepareStatement(query);
        //statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return buildList(resultSet, "categoryName");
    }
  }
  

  
  //  public List<String> getOwnerList(String vin)
//      throws SQLException, IOException {
//    String query = SQL.getSQL("owner-query");
//    try (Connection carsConnection = carsSource.getConnection()) {
//      PreparedStatement statement
//          = carsConnection.prepareStatement(query);
//      statement.setString(1, vin);
//      ResultSet resultSet = statement.executeQuery();
//      return buildList(resultSet, "name");
//    }
//  }
//
//}
  
//  public List<Integer> getCurrentScores() {
//      return Arrays.asList(scoresByCategory.values().toArray(new Integer[0]));
//  }
  
  /**
   * getScores(User userBean) returns a list of the scores by category in 
 the following order: Computer Science, Mathematics, Physics, Astronomy.
   * 
     * @param scoreList
   * @param userBean = the current user
     * @return 
   */
  
//  public List<Integer> getScores() {
//      FacesContext facesContext = FacesContext.getCurrentInstance();
//UserBean userBean
//    = (User) facesContext.getApplication()
//    .getVariableResolver().resolveVariable(facesContext, "userBean");
//      return Arrays.asList(scoresMaster.get(userBean.getUsername()).values().toArray(new Integer[0]));
//  }
  
public Map<String, Integer> getScoresByCategory() {
    //return Arrays.asList(scoresByCategory.values()).toArray();
    return scoresByCategory;
}

public void setScoresByCategory(QuizBean qb) {
    User currentUser = qb.getCurrentUser();
    String category = qb.getCurrentCategory();
    
}
}



//  public List<String> getVinList()
//      throws SQLException, IOException {
//    try (Connection carsConnection = carsSource.getConnection()) {
//      Statement statement = carsConnection.createStatement();
//      ResultSet resultSet = statement.executeQuery(SQL.getSQL("vin-query"));
//      return buildList(resultSet, "VIN");
//    }
//  }
//
//  public Car getCar(String vin)
//      throws SQLException, IOException {
//    String query = SQL.getSQL("car-query");
//    try (Connection carsConnection = carsSource.getConnection()) {
//      PreparedStatement statement
//          = carsConnection.prepareStatement(query);
//      statement.setString(1, vin);
//      ResultSet resultSet = statement.executeQuery();
//      return buildCar(resultSet);
//    }
//  }
//
//  public List<String> getOwnerList(String vin)
//      throws SQLException, IOException {
//    String query = SQL.getSQL("owner-query");
//    try (Connection carsConnection = carsSource.getConnection()) {
//      PreparedStatement statement
//          = carsConnection.prepareStatement(query);
//      statement.setString(1, vin);
//      ResultSet resultSet = statement.executeQuery();
//      return buildList(resultSet, "name");
//    }
//  }
//
//}
