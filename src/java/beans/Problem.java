package beans;

import java.io.Serializable;

//@Named
//@SessionScoped
public class Problem implements Serializable {

  private String question, correctAnswer, categoryName;
  private int correctAnswerIndex;
  private String[] choices;


//    public Problem(String categoryName, String firstQuestion, ArrayList<String> choices, String correctAnswer) {

  public Problem() {
      
  }
  
  public Problem(String categoryName, String newQuestion, String[] answers, String correctAnswer) {
      this.categoryName = categoryName;
      this.question = newQuestion;
      this.choices = answers;
      //this.correctAnswerIndex = cAnswerIndex; 
      this.correctAnswer = correctAnswer;
  }
  
//
//  public int getCorrectAnswerIndex() {
//      return this.correctAnswerIndex;
//  }
  
  public String getCorrectAnswer() {
      return correctAnswer;
  }
  
//  public void setCorrectAnswer(String cAnswer) {
//      this.correctAnswer= cAnswer;
//  }
  public String getCategoryName() {
      return categoryName;
  }
  
  public void setQuestion(String newQuestion) {
      this.question = newQuestion;
  }
  
  public String getQuestion() {
      return this.question;
  }
  
  public void setCategoryName(String name) {
      this.categoryName = name;
  }

  
//  public String getCorrectAnswer() {
//      return this.correctAnswer;
//  }

  public String[] getChoices() {
//  public String[] getAnswers() {
  //public String[] getAnswers() {
//      ArrayList<String> stringList = new ArrayList<>();
//      for (int i = 0; i < 4; ++i) {
//          stringList.add(choices[i]);
//      }
//      //return this.choices;
//      return stringList;
return this.choices;
  }
  
  public void setChoices(String[] answerArray) {
      for (int i = 0; i < 4; ++i) {
          choices[i] = answerArray[i];
      }
  }

  // override for more sophisticated checking
  public boolean isCorrect(String response) {
    return response.trim().equalsIgnoreCase(correctAnswer.trim());
  }

}
