package beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class QuizBean implements Serializable {

  private int currentIndex;
  private int score, numQuestions;
  private String response = "";
  private String correctAnswer, currentCategory, currentQuestion, lastCorrectAnswer;
  private ArrayList<Problem> problems = new ArrayList<>();
  private Problem currentProblemBean;
  private boolean gotLastQuestionRight, outOfQuestions, isFirstQuestion, isLastQuestion;
  //private Database db = new Database();
  @Inject 
  private Database db;
  private ArrayList<Problem> problemList = new ArrayList<>();
  private List<String> answers = new ArrayList<>();
  private ArrayList<String> categories = new ArrayList<>();
  private User currentUser;
  private int tries;

  public QuizBean() {
    }
  
  private void setProblems(ArrayList<Problem> newProb) {
      problems = newProb;
      currentIndex = 0;
      score = 0;
  }
  
    public String beginAction() {
        problems.clear();
        db = new Database();
        problems = (ArrayList<Problem>) db.getProblems(this.currentCategory);
        setCurrentIndex(0);
        setCurrentProblemBean(getCurrentIndex());
        setCurrentQuestion(currentProblemBean.getQuestion());
        this.isFirstQuestion = true;
        this.isLastQuestion = false;
      return "quiz";
  }
    
    public void setCurrentUser(User userBean) {
        this.currentUser = userBean;
    }
    
    public User getCurrentUser() {
        return this.currentUser;
    }
  
    public String checkAnswerAction() throws SQLException, IOException {
        tries++;
    if (problems.get(currentIndex).isCorrect(response)) {
        setScore(score + 1);
        db.updateScore(currentUser, this);
        currentUser.updateScore();
        setGotLastQuestionRight(true);
        if (((problems.size() - 1) > currentIndex)) {
            nextProblem();
            setIsFirstQuestion(false);
            setIsLastQuestion(false);
            return "quiz";
        } else {
            setIsFirstQuestion(false);
            setIsLastQuestion(true);
            setLastCorrectAnswer(correctAnswer);
            return "quiz";
        }
    } else {
        setGotLastQuestionRight(false);
        setLastCorrectAnswer(correctAnswer);
        if (((problems.size() - 1) > currentIndex)) {
            nextProblem();
            setIsFirstQuestion(false);
            setIsLastQuestion(false);
            return "quiz";
        } else {
            setIsFirstQuestion(false);
            setIsLastQuestion(true);
            return "quiz";
        }
      }
    }

    public String endAction() {
        setLastCorrectAnswer(lastCorrectAnswer);
        setCurrentIndex(0);
        response = "";
        numQuestions = 0;
        Collections.shuffle(problems);
        setOutOfQuestions(false);
        currentIndex = 0;
        answers.clear();
        problems.clear();
        tries = 0;
        score = 0;
        return "select-category";
    }

  private String nextProblem() {
    answers.clear();
    ++numQuestions;
    setLastCorrectAnswer(currentProblemBean.getCorrectAnswer());
    ++currentIndex;
    ++tries;
    response = "";
    if (!(currentIndex < problems.size() - 1)) {
        setCurrentProblemBean(currentIndex);
        setCurrentQuestion(currentProblemBean.getQuestion());
        return "quiz";
    } else {
        setCurrentProblemBean(currentIndex);
        setCurrentQuestion(currentProblemBean.getQuestion());
        setIsLastQuestion(true);
        return "quiz";
    }
  }
  
    public List<String> getAnswers() {
        return Arrays.asList(currentProblemBean.getChoices());
    }
  
  public int getNumQuestions() {
      return this.numQuestions;
  }
  
  public void setNumQuestions(int num) {
      this.numQuestions = num;
  }

  public String getCorrectAnswer() {
        return problems.get(currentIndex).getCorrectAnswer();
  }

  public String getResponse() {
    return response;
  }

  public void setResponse(String newValue) {
    response = newValue;
  }
  
  public void setCurrentProblemBean(int newIndex) {
      if (problems.isEmpty()) {
         problems = (ArrayList<Problem>) db.getProblems(this.currentCategory);
      }
      this.currentProblemBean = problems.get(newIndex);
      
  }
  
  public Problem getCurrentProblem() {
    return problems.get(currentIndex);
  }
  
  public void setScore(int nScore) {
      this.score = nScore;
  }
  
    public int getScore() {
    return score;
  }
  
  public void setGotLastQuestionRight(boolean val) {
      this.gotLastQuestionRight = val;
  }
  
  public boolean getGotLastQuestionRight() {
      return this.gotLastQuestionRight;
  }
  
  public void setCurrentIndex(int number) {
      this.currentIndex = number;
  }
  
  public int getCurrentIndex() {
      return this.currentIndex;
  }
   
  public void setCurrentProblemBean(Problem newP) {
      this.currentProblemBean = newP;
  }
  
    public Problem getCurrentProblemBean() {
      if (currentIndex >= problems.size()) {
          System.err.println("problem in getCurrentProblem");
          return null;
      } else {
          if (problems.isEmpty()) {
              return this.problems.get(currentIndex);
          } else {
            return this.problems.get(currentIndex);
          }
      }
  }

  public void setOutOfQuestions(boolean out) {
      this.outOfQuestions = out;
  }
  
  public boolean getOutOfQuestions() {
      return this.outOfQuestions;
  }
  
  public String getLastCorrectAnswer() {
      return this.lastCorrectAnswer;
  }
  
  public void setLastCorrectAnswer(String ans) {
      this.lastCorrectAnswer = ans;
  }

    public boolean getIsFirstQuestion() {
        return isFirstQuestion;
    }

    public void setIsFirstQuestion(boolean isFirstQuestion) {
        this.isFirstQuestion = isFirstQuestion;
    }

    public boolean getIsLastQuestion() {
        return isLastQuestion;
    }

    public void setIsLastQuestion(boolean isLastQuestion) {
        this.isLastQuestion = isLastQuestion;
    }

    public boolean isGotLastQuestionRight() {
        return gotLastQuestionRight;
    }

    public boolean isOutOfQuestions() {
        return outOfQuestions;
    }
    public String getAnswer() {
        return "";
    }
    
    public void setAnswer(String newAns) {
        try {
            int answer = Integer.parseInt(newAns.trim());
            if (this.getCurrentProblem().getCorrectAnswer().equals(correctAnswer)) {
                ++score;
            }
            currentIndex = (currentIndex + 1) % problems.size();
        } catch (NumberFormatException ex) {
            
        }
    }
    
    public String getCurrentQuestion() {
        if (this.currentIndex >= problems.size()) {
            System.err.println("error with getCurrentQuestion");
            return null;
        } else {
            return currentProblemBean.getQuestion();
        }
    }
    
    public String getQuestion() {
        return problems.get(currentIndex).getQuestion();
    }
    
    public void setCurrentQuestion(String newQuestion) {
        this.currentQuestion = newQuestion;
    }
    
    public ArrayList<String> getCategories() {
        return categories;
    }
   
    public String getCurrentCategory() {
        return currentCategory;
    }
    
    public void setCurrentCategory(String newCategory) {
        this.currentCategory = newCategory;
    }

}
