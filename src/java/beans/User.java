/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author LauraKrebs
 */

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

@Named("user")
@ManagedBean
@SessionScoped
public class User implements Serializable {
    private String username;
    private String password, passwordCheck;
      @Inject 
  private Database db;
      private QuizBean qb = new QuizBean();
      private Map<String, Integer> scores;
      
      public User() {
          
      }
      public User(String userName, String password) {
        this.db = new Database();
          this.username = userName;
          this.password = password;
          scores = new HashMap<String, Integer>();   
          //this.qb = new QuizBean();
          
      }
      
      public void init() throws SQLException, IOException {
          this.db = new Database();
          this.qb = new QuizBean();
          this.username = username;
          this.password = password;
          scores = new HashMap<String, Integer>();   
          List<Integer> scoreList = new ArrayList<Integer>();
//          for (String category : db.getCategoryNames()) {
//              scoreList.add(0);
//          }
            for (int i = 0; i < qb.getCategories().size(); ++i) {
                scoreList.add(0);
            }
          db.addUser(this);
          int i = 0;
          for (String categoryName : db.getCategoryNames()) {
              scores.put(categoryName, scoreList.get(i));
              ++i;
          }
          i = 0;
          db.setCurrentScores(scoreList);
          //this.qb = new QuizBean();
      }
      
      public void updateScore() {
          scores.put(qb.getCurrentCategory(), db.getScoresByCategory().get(qb.getCurrentCategory() + 1));
      } 

  public String getUsername() {
    return username;
  }

  public void setUsername(String newValue) {
    username = newValue;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String newValue) {
    password = newValue;
  }
  
  public String getPasswordCheck() {
      return passwordCheck;
  }
  
  public void setPasswordCheck(String pw) {
      this.passwordCheck = pw;
  }
  
  public String verifyPassword() throws SQLException {
      if (getPassword().trim().equals(passwordCheck.trim())) {
          db.addUser(this);
          qb.setCurrentUser(this);
          return "select-category";
      } else {
          return "registration";
      }
  }
  
  
}


