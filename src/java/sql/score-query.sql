/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  LauraKrebs
 * Created: Feb 17, 2016
 */


SELECT "score"."correctAnswerCount" FROM "users", "category", "correctAnswerCount"
WHERE
  "score"."users" = "users"."id"
  AND "score"."category" = "category"."id"
  AND "score"."correctAnswerCount" = ?

