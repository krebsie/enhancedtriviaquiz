/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  LauraKrebs
 * Created: Feb 15, 2016
 */



CREATE TABLE category (
    id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    "category_name" VARCHAR(100) NOT NULL PRIMARY KEY
);