/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  LauraKrebs
 * Created: Feb 15, 2016
 */
INSERT INTO category ("category_name")
    VALUES ('Computer Science');

INSERT INTO category ("category_name")
    VALUES ('Mathematics');

INSERT INTO category ("category_name")
    VALUES ('Physics');

INSERT INTO category ("category_name")
    VALUES ('Astronomy');
