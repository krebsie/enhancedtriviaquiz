/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  LauraKrebs
 * Created: Feb 15, 2016
 */
CREATE TABLE "score" (
    "id" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
  "users" INTEGER NOT NULL,
  "category" INTEGER NOT NULL,
  "correctAnswerCount" INTEGER
);
