/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  LauraKrebs
 * Created: Feb 15, 2016
 */

INSERT INTO "score" ("user", "category")
  VALUES (
    (SELECT "user"."username" FROM "user"
      WHERE "person"."name" = 'Doe, Janet'),
    (SELECT "car"."id" FROM "car"
      WHERE "car"."vin" = '0123456789abcdefg')
  );



